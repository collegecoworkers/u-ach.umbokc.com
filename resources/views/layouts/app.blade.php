<!DOCTYPE html>
<html lang="en">
<head>
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" href="http://cdn.uwebu.ru/u-emmet-attr/src/ea.css?v=1.2">
	<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
	<link rel="stylesheet" href="/assets/css/style.default.css" id="theme-stylesheet">
	<link rel="stylesheet" href="/assets/css/grasp_mobile_progress_circle-1.0.0.min.css">
	<link rel="stylesheet" href="/assets/css/custom.css">
	<link rel="shortcut icon" href="/assets/img/favicon.ico">
	<script src="https://use.fontawesome.com/99347ac47f.js"></script>
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link rel="stylesheet" href="https://file.myfontastic.com/da58YPMQ7U5HY8Rb6UxkNf/icons.css">
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body ea>
	
	@include('layouts.nav')

	<div class="page home-page">
		<header class="header">
			<nav class="navbar">
				<div class="container-fluid">
					<div class="navbar-holder d-flex align-items-center justify-content-between">
						<div class="navbar-header">
							<a id="toggle-btn" href="#" class="menu-btn"><i class="icon-bars"> </i></a>
							<a href="/" class="navbar-brand"><h3>{{ config('app.name', 'Laravel') }}</h3></a>
							</div>
						<ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
							<li class="nav-item"><a class="nav-link logout" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Выйти <i class="fa fa-sign-out"></i></a></li>
						</ul>
					</div>
				</div>
			</nav>
		</header>
		@yield('content')
		<footer class="main-footer">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-6">
						<p>&copy; {{ config('app.name', 'Laravel') }}. Все права защищены.</p>
					</div>
				</div>
			</div>
		</footer>
	</div>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="/assets/js/tether.min.js"></script>
	<script src="/assets/js/bootstrap.min.js"></script>
	<script src="/assets/js/jquery.cookie.js"> </script>
	<script src="/assets/js/grasp_mobile_progress_circle-1.0.0.min.js"></script>
	<script src="/assets/js/jquery.nicescroll.min.js"></script>
	<script src="/assets/js/jquery.validate.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
	<script src="/assets/js/charts-home.js"></script>
	<script src="/assets/js/front.js"></script>
</body>
</html>
