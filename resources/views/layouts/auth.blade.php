<!DOCTYPE html>
<html lang="en">
<head>
	<title>{{ config('app.name', 'Laravel') }}</title><meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
	<link rel="stylesheet" href="/assets/css/style.default.css" id="theme-stylesheet">
	<link rel="stylesheet" href="/assets/css/grasp_mobile_progress_circle-1.0.0.min.css">
	<link rel="stylesheet" href="/assets/css/custom.css">
	<link rel="shortcut icon" href="/assets/img/favicon.ico">
	<script src="https://use.fontawesome.com/99347ac47f.js"></script>
	<link rel="stylesheet" href="https://file.myfontastic.com/da58YPMQ7U5HY8Rb6UxkNf/icons.css">
	<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>
<body>
	<div class="page login-page">
		<div class="container">
			<div class="form-outer text-center d-flex align-items-center">
				<div class="form-inner" style="width: 600px">
					<div class="logo text-uppercase">{{ config('app.name', 'Laravel') }}</div>
					@yield('content')
				</div>
				<div class="copyrights text-center">
					<p>&copy; {{ config('app.name', 'Laravel') }}</p>
				</div>
			</div>
		</div>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="/assets/js/tether.min.js"></script>
	<script src="/assets/js/bootstrap.min.js"></script>
	<script src="/assets/js/jquery.cookie.js"> </script>
	<script src="/assets/js/grasp_mobile_progress_circle-1.0.0.min.js"></script>
	<script src="/assets/js/jquery.nicescroll.min.js"></script>
	<script src="/assets/js/jquery.validate.min.js"></script>
	<script src="/assets/js/front.js"></script>
</body>

</html>
