@php
use App\{User};
$curr = User::curr();
$r = User::curRole();
$nav = [
	['visible' => true, 'url' => route('/'), 'icon' => 'home', 'label' => 'Главная'],
	['visible' => true, 'url' => route('/task/all'), 'icon' => 'form', 'label' => 'Все задания'],
	['visible' => $r != 'admin', 'url' => route('/task/my'), 'icon' => 'presentation', 'label' => 'Мои задания'],
	['visible' => $r == 'admin', 'url' => route('/users'), 'icon' => 'user', 'label' => 'Пользователи'],
];
@endphp

<nav class="side-navbar">
	<div class="side-navbar-wrapper">
		<div class="sidenav-header d-flex align-items-center justify-content-center">
			<div class="sidenav-header-inner text-center">
				<p class="rounded-circle" bgc#e d:b ta:c m:a style="line-height: 30px; width: 30px;height: 30px;">{{ ucfirst($curr->name[0]) }}</p>
				<h2 class="h5 text-uppercase">{{ $curr->full_name }}</h2>
				<span class="text-uppercase">{{ $curr->getRole() }}</span>
			</div>
			<div class="sidenav-header-logo"><a href="/" class="brand-small text-center"> <strong>{{ ucfirst($curr->name[0]) }}</strong></a></div>
		</div>
		<div class="main-menu">
			<ul id="side-main-menu" class="side-menu list-unstyled">                  
				@foreach ($nav as $item)
					@if ($item['visible'])
						<li class="{{ url()->current() == $item['url'] ? 'active' : ''}}">
							<a href="{{ $item['url'] }}">
								<i class="icon-{{$item['icon']}}"></i>
								<span>{{$item['label']}}</span>
							</a>
						</li>
					@endif
				@endforeach
			</ul>
		</div>
	</div>
</nav>
