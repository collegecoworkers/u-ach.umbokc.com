@extends('../layouts.app')
@section('content')
<section class="dashboard-header section-padding">
	<div class="container-fluid">
		<div class="row d-flex align-items-md-stretch">
			<div class="col-lg-12 col-md-12">
				<div class="wrapper to-do">
					<header>
						<h2 class="display h4">Пользователи</h2>
					</header>
					<a href="{{ route('/user/add') }}" class="btn btn-primary">Добавить</a>
					<div class="card-block">
						<table class="table">
							<thead>
								<tr>
						<th>#</th>
									<th>Имя</th>
									<th>Логин</th>
									<th>Email</th>
									<th>Статус</th>
									<th>Действия</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($users as $item)
									<tr>
									<td ta:c>{{$item->id}}</td>
									<td ta:c>{{$item->full_name}}</td>
									<td ta:c>{{$item->name}}</td>
									<td ta:c>{{$item->email}}</td>
									<td ta:c>{{$item->getRole()}}</td>
										<td>
											<a href="{{ route('/user/edit/{id}', ['id'=>$item->id]) }}">
												<i class="fa fa-pencil"></i>
											</a>
											<a href="{{ route('/user/delete/{id}', ['id'=>$item->id]) }}" onclick="return confirm('Вы уверенны?')">
												<i class="fa fa-trash"></i>
											</a>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
