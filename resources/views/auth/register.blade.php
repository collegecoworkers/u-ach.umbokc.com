@extends('../layouts.auth')
@section('content')
<p>Регистрация</p>
<form id="login-form" method="post" action="{{ route('register') }}">
	{{ csrf_field() }}
	<div class="form-group">
		<label for="login-full_name" class="label-custom">Имя</label>
		<input id="login-full_name" type="text" name="full_name" required="">
		@if ($errors->has('full_name'))<span class="help-block"><strong c#f>{{ $errors->first('full_name') }}</strong></span> @endif
	</div>
	<div class="form-group">
		<label for="login-name" class="label-custom">Логин</label>
		<input id="login-name" type="text" name="name" required="">
		@if ($errors->has('name'))<span class="help-block"><strong c#f>{{ $errors->first('name') }}</strong></span> @endif
	</div>
	<div class="form-group">
		<label for="login-email" class="label-custom">Email</label>
		<input id="login-email" type="text" name="email" required="">
		@if ($errors->has('email'))<span class="help-block"><strong c#f>{{ $errors->first('email') }}</strong></span> @endif
	</div>
	<div class="form-group">
		<label for="login-password" class="label-custom">Пароль</label>
		<input id="login-password" type="password" name="password" required="">
		@if ($errors->has('password'))<span class="help-block"><strong c#f>{{ $errors->first('password') }}</strong></span> @endif
	</div>
	<div class="form-group">
		<label for="login-password1" class="label-custom">Пароль еще раз</label>
		<input id="login-password1" type="password" name="password_confirmation" required="">
		@if ($errors->has('password_confirmation'))<span class="help-block"><strong c#f>{{ $errors->first('password_confirmation') }}</strong></span> @endif
	</div>
	<input type="submit" class="btn btn-success" value="Зарегистрироваться" />
</form>
<a href="{{ route('login') }}" class="forgot-pass">Войти</a>
@endsection
