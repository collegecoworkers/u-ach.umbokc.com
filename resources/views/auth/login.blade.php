@extends('../layouts.auth')
@section('content')
<p>Вход</p>
<form id="login-form" method="post" action="{{ route('login') }}">
	{{ csrf_field() }}
	<div class="form-group">
		<label for="login-email" class="label-custom">Email</label>
		<input id="login-email" type="text" name="email" required="">
		@if ($errors->has('email'))<span class="help-block"><strong c#f>{{ $errors->first('email') }}</strong></span> @endif
	</div>
	<div class="form-group">
		<label for="login-password" class="label-custom">Пароль</label>
		<input id="login-password" type="password" name="password" required="">
		@if ($errors->has('password'))<span class="help-block"><strong c#f>{{ $errors->first('password') }}</strong></span> @endif
	</div>
	<input type="submit" class="btn btn-success" value="Войти" />
</form>
<a href="{{ route('register') }}" class="forgot-pass">Зарегистрироваться</a>
@endsection
