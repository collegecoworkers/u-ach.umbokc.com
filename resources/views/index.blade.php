@php
	use App\User;
	use App\Task;
	$curr = User::curr();
@endphp
@extends('layouts.app')
@section('content')
<section class="dashboard-counts section-padding">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-3 col-md-4 col-6">
				<div class="wrapper count-title d-flex">
					<div class="icon"><i class="icon-user"></i></div>
					<div class="name"><strong class="text-uppercase">Пользователей</strong>
						<div class="count-number">{{ User::count() }}</div>
					</div>
				</div>
			</div>
			<div class="col-xl-3 col-md-4 col-6">
				<div class="wrapper count-title d-flex">
					<div class="icon"><i class="icon-padnote"></i></div>
					<div class="name"><strong class="text-uppercase">Всего заданий</strong>
						<div class="count-number">{{ Task::count() }}</div>
					</div>
				</div>
			</div>
			<div class="col-xl-3 col-md-4 col-6">
				<div class="wrapper count-title d-flex">
					<div class="icon"><i class="icon-list-1"></i></div>
					<div class="name"><strong class="text-uppercase">Завершено заданий</strong>
						<div class="count-number">{{ Task::where('status', 'done')->count() }}</div>
					</div>
				</div>
			</div>
			<div class="col-xl-3 col-md-4 col-6">
				<div class="wrapper count-title d-flex">
					<div class="icon"><i class="icon-check"></i></div>
					<div class="name"><strong class="text-uppercase">Ваших заданий</strong>
						@if (User::isWorker())
							<div class="count-number">{{ Task::where('worker_id', $curr->id)->count() }}</div>
						@else
							<div class="count-number">{{ Task::where('employer_id', $curr->id)->count() }}</div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="dashboard-header section-padding">
	<div class="container-fluid">
		<div class="row d-flex align-items-md-stretch">
			<div class="col-lg-12 col-md-12">
				<div class="wrapper to-do">
					<header>
						<h2 class="display h4">Последние добавленные задания</h2>
					</header>
					<div class="card-block">
						<table class="table">
							<thead>
								<tr>
									<th>#</th>
									<th>Название</th>
									<th>Работодатель</th>
									<th>Работник</th>
									<th>Статус</th>
									<th>Действия</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($items as $item)
									<tr>
										<td>{{ $item->id }}</td>
										<td>{{ $item->title }}</td>
										<td>{{ $item->getEmp()->full_name }}</td>
										<td>{{ $item->getWorName() }}</td>
										<td>{{ $item->getStatus() }}</td>
										<td>
											<a href="{{ route('/task/view/{id}', ['id' => $item->id]) }}">
												<i class="fa fa-eye"></i>
											</a>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
