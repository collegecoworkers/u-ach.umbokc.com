@extends('../layouts.app')
@section('content')
<section class="dashboard-header section-padding">
	<div class="container-fluid">
		<div class="row d-flex align-items-md-stretch">
			<div class="col-lg-12 col-md-12">
				<div class="wrapper to-do">
					<header>
						<h2 class="display h4">Изменить задание</h2>
					</header>
					<div class="card-block">
						@include('task._form')
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
