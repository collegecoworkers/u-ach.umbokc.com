@extends('../layouts.app')
@section('content')
<section class="dashboard-header section-padding">
	<div class="container-fluid">
		<div class="row d-flex align-items-md-stretch">
			<div class="col-lg-12 col-md-12">
				<div class="wrapper to-do">
					<header>
						<h2 class="display h4">Отправка на проверку</h2>
					</header>
					<div class="card-block">
						{!! Form::open(['enctype'=>'multipart/form-data', 'url' => '/task/to-complete/'.$model->id ]) !!}
							<div class="form-group">
								<label>Коммент:</label>
								<textarea class="form-control" name="comment"></textarea>
							</div>
							<div class="form-group">
								<label>Файл:</label>
								{!! Form::file('file', ['placeholder' => 'Файл','class' => 'form-control']) !!}
							</div>
							<div class="form-actions">
								<button type="submit" class="btn btn-success">Сохранить</button>
							</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
