@php
	use App\User;
	$curr = User::curr();
@endphp
@extends('../layouts.app')
@section('content')
<section class="dashboard-header section-padding">
	<div class="container-fluid">
		<div class="row d-flex align-items-md-stretch">
			<div class="col-lg-12 col-md-12">
				<div class="wrapper to-do">
					<header>
						<h2 class="display h4">Мои задания</h2>
					</header>
					<div class="card-block">
						<table class="table">
							<thead>
								<tr>
									<th>#</th>
									<th>Название</th>
									<th>Работодатель</th>
									<th>Статус</th>
									<th>Действия</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($items as $item)
									<tr>
										<td>{{ $item->id }}</td>
										<td>{{ $item->title }}</td>
										<td>{{ $item->getEmp()->full_name }}</td>
										<td>{{ $item->getStatus() }}</td>
										<td>
											@if ($item->status == 'in')
												<a href="{{ route('/task/complete/{id}', ['id' => $item->id]) }}">
													Отправить на проверку
												</a>
											@endif
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
