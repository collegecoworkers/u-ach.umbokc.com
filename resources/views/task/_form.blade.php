{!! Form::open(['enctype'=>'multipart/form-data', 'url' => isset($model) ? '/task/update/'.$model->id : '/task/create']) !!}
	<div class="form-group">
		<label>Название:</label>
		<input type="text" class="form-control" name="title" value="{{ isset($model) ? $model->title : ''}}" />
	</div>
	<div class="form-group">
		<label>Описание:</label>
		<textarea class="form-control" name="desc">{{ isset($model) ? $model->desc : ''}}</textarea>
	</div>
	<div class="form-group">
		<label>Файл:</label>
		{!! Form::file('file', ['placeholder' => 'Файл','class' => 'form-control']) !!}
	</div>
	@isset($model)
		<div class="form-group">
			<label>Статус:</label>
			{!! Form::select('status', $statuss,  $model->status, ['class' => 'form-control']) !!}
		</div>
	@endisset
	<div class="form-actions">
		<button type="submit" class="btn btn-success">Сохранить</button>
	</div>
{!! Form::close() !!}
