@extends('../layouts.app')
@section('content')
<section class="dashboard-header section-padding">
	<div class="container-fluid">
		<div class="row d-flex align-items-md-stretch">
			<div class="col-lg-12 col-md-12">
				<div class="wrapper to-do">
					<header>
						<h2 class="display h4">Мои задания</h2>
					</header>
					<a href="{{ route('/task/add') }}" class="btn btn-primary">Добавить</a>
					<div class="card-block">
						<table class="table">
							<thead>
								<tr>
									<th>#</th>
									<th>Название</th>
									<th>Работник</th>
									<th>Статус</th>
									<th>Действия</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($items as $item)
									<tr>
										<td>{{ $item->id }}</td>
										<td>{{ $item->title }}</td>
										<td>{{ $item->getWorName() }}</td>
										<td>{{ $item->getStatus() }}</td>
										<td>
											<a href="{{ route('/task/view/{id}', ['id'=>$item->id]) }}">
												<i class="fa fa-eye"></i>
											</a>
											<a href="{{ route('/task/edit/{id}', ['id'=>$item->id]) }}">
												<i class="fa fa-pencil"></i>
											</a>
											<a href="{{ route('/task/delete/{id}', ['id'=>$item->id]) }}" onclick="return confirm('Вы уверенны?')">
												<i class="fa fa-trash"></i>
											</a>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
