@extends('../layouts.app')
@section('content')
<section class="dashboard-header section-padding">
	<div class="container-fluid">
		<div class="row d-flex align-items-md-stretch">
			<div class="col-lg-12 col-md-12">
				<div class="wrapper to-do">
					<header>
						<h2 class="display h4">{{ $model->title }}</h2>
					</header>
					<div class="card-block">
						<b>Описание: </b>
						<p>{!! $model->title !!}</p>
						
						@if ($model->path != '')
							<b>Исходный файл: </b>
							<br>
							<a href="{{ $model->getPath() }}" target="_blank">Скачать</a>
							<br>
							<br>
						@endif
						@if ($cur_role == 'worker' && ($model->status == 'added'))
							<p>
								<a href="{{ route('/task/get/{id}', ['id' => $model->id ]) }}" class="btn btn-primary">Взять задание</a>
							</p>
						@else
							<b>Работник: </b>
							<p>{{ $model->getWorName() }}</p>
							@if ($cur_role == 'employer')
								<b>Коммент: </b>
								<p>{{ $model->comment }}</p>
								@if ($model->path2 != '')
									<b>Файл от работника: </b>
									<br>
									<a href="{{ $model->getPath2() }}" target="_blank">Скачать</a>
								@endif
							@endif
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
