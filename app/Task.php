<?php

namespace App;

class Task extends MyModel{

	public function getPath() {
		// return $this->getFolder() . $this->path;
		return $this->path == '' ? '' : '/files/' . $this->path;
	}

	public function getPath2() {
		// return $this->getFolder() . $this->path2;
		return $this->path2 == '' ? '' : '/files/' . $this->path2;
	}

	public function saveFile() {
		if (request()->file == '')
			return;

		$fName = time().'.'.request()->file->getClientOriginalExtension();
		request()->file->move($this->getFolder(), $fName);
		$this->path = $fName;
	}

	public function saveFile2() {
		if (request()->file == '')
			return;

		$fName = time().'.'.request()->file->getClientOriginalExtension();
		request()->file->move($this->getFolder(), $fName);
		$this->path2 = $fName;
	}

	public function getFolder() {
		return public_path()."/files/";
	}

	public function getEmp() {
		return User::getbyId($this->employer_id);
	}

	public function getWor() {
		return User::getbyId($this->worker_id);
	}

	public function getWorName() {
		$w = $this->getWor();
		return $w ? $w->full_name : 'Нет';
	}

	public function getStatus() {
		return self::getStatusOf($this->status);
	}

	public static function getStatuss(){
		return [
			'added' => 'Добавленно',
			'in' => 'В работе',
			'checking' => 'На проверке',
			'done' => 'Завершено',
			'false' => 'Отклоненно',
		];
	}

	public static function getStatusOf($r){
		$statuss = self::getStatuss();
		if(array_key_exists($r, $statuss)) 
			return $statuss[$r];
		return $statuss[0];
	}
}
