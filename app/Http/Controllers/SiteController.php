<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	User,
	Task
};

class SiteController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}

	public function Index() {
		$this->the_before();
		$user = User::curr();
		return view('index')->with([
			'cur_role' => $this->cur_role,
			'items' => Task::limit(7)->orderBy('id', 'desc')->get() ,
		]);
	}
	public function All() {
		$this->the_before();
		$user = User::curr();
		return view('index')->with([
			'cur_role' => $this->cur_role,
			'items' => Task::all() ,
		]);
	}

	public function the_before() {
		$this->cur_role = User::curRole();
	}
}
